# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from drealtime import iShoutClient
ishout_client = iShoutClient()


# Create your views here.

@login_required
def home(request):
    users = User.objects.exclude(id=request.user.id)

    # variables = RequestContext(request, {'users': users})

    return render_to_response('home.html', {'users': users})


@login_required
def alert(request):
    r = request.GET
    user = request.GET.get('user', '0')

    ishout_client.emit(
        int(user),
        'alertchannel',
        data={'msg': 'Hello Dear friend)'}
    )

    return redirect('home')

